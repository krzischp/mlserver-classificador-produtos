[GitLab link](https://gitlab.com/krzischp/mlserver-classificador-produtos/-/tree/main)

# First commit and tag:
```bash
git init --initial-branch=main
git remote add origin https://gitlab.com/krzischp/mlserver-classificador-produtos.git
git add .
git commit -m "Initial commit"
git push -u origin main
git tag -a v1.0.0 -m "Primeiro release"
git push --tags
```

# How to tag and update a new version:
```bash
git commit -am "Corrigindo modelo para tratamento de acentos" 
git push
git tag -a v1.0.4 -m "Pequena correção"
git push --tags
```

[Referencia](https://aurimrv.gitbook.io/pratica-devops-com-docker-para-machine-learning/)
